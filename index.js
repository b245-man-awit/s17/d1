// console.log("Heloo world");

// [SECTION] Functions
	// Functions in javascript are line/blocks of codes that tell our device/application to perform a specific task when called/invoked.
	// It prevents repeating lines/blocks of codes that perform the same task/function.

	// Function Declarations
		// function statement is the definition of a function.

	/*
		Syntax:
			function functionName()
				code block (statement)
			}

		* function keyword - use to define a javascript function.
		* functionName - name of the function, which will be use to call/invoke the function
		* function block({}) - indicates the function body.	
	
	*/

	function printName(){
		console.log("My name is John");
	}

// Function Invocation
	// This run/execute the code block inside the function.

	printName();

	declaredFunction(); // we cannot invoked a function that we have not declared/defined yet.

// [SECTION] Function Declarations vs Function Expressions
	
	// Function Declaration
		// function can be created by using function keyword and adding a function name.
		// "saved for later used"
		// Declared functions cane be "hoisted", as long a function has been defined.
			// Hoisting is JS behavior for certain variables (var) and functions to run or use them before their declaration.

	function declaredFunction(){
		console.log("Hello World from declaredFunction()");
	}

	// Function Expression
		//a function can also be stored in a variable.

		/*
			Syntax:
				let/const variableName = function(){
					//code block (statement)
				}

				- function(){ - Anonymous function, a function without a name.

		*/

		// variableFunction(); //error - function expression, being stored in a let/const variable, cannot be hoisted.

		let variableFunction = function(){
			console.log("Hello Again!");
		}

		variableFunction();

		// we create also function expression with a named functions.

		let funcExpression = function funcName(){
			console.log("Hello from the other side.");
		}

		// funcName(); // funcName() is not defined

		funcExpression(); // to invoke the function, we invoked it by its variable name and not by its function name

		// Reassign declared functions and function expression to a new anonymous function

		declaredFunction = function(){
			console.log("updated declaredFunction");
		}

		declaredFunction();

		funcExpression = function(){
			console.log("updated funcExpression");
		}

		funcExpression();

		// we cannot re-asssign a function expression initialized with const.

		const constantFunc = function(){
			console.log("Initialized with const")
		}

		constantFunc();

		// This will result to reassignment error
		// constantFunc = function(){
		// 	console.log("Cannot be reassigned");
		// }

		// constantFunc();

// [SECTION] Function Scoping 
		/*
			Scope is the accessibility (visibility) of variables.

			Javascript variables ha 3 types of scope:
			1. global scope
			2. local/block scope
			3. function scope

		*/

		//Global scope
			//variable can be access anywhere from the program

		let globalvar = "Mr. Worldwide"

		//Local scope

		 //console.log(localVar);

		{
			// var localVar = "Armando Peres";
			let localVar = "Armando Peres";
		}

		console.log(globalvar);
		//console.log(localVar);

		//Function Scope
			// Each function creates a new scope.
			// variables defined inside a function are not accessible from outside the function.

			function showNames(){
					// Function Scope Variable
					var functionVar = "Joe";
					const functionConst = "John";
					let functionLet = "Joey";

					console.log(functionVar);
					console.log(functionConst);
					console.log(functionLet);
				}		

				showNames();

				// console.log(functionVar);
				// console.log(functionConst);
				// console.log(functionLet);	


		function myNewFunction(){
			let name = "jane";

			function nestedFunc(){
				let nestedFunc = "John"
				console.log(name)
			}

			// console.log(nestedName);
			nestedFunc();
		}

		myNewFunction();

		// nestedFunc(); // result to an error.

		// Global scope Variable

		let globalName = "Alex";

		function myNewFunction2(){
			let	nameInside = "Renz";
			console.log(globalName);
		}

		myNewFunction2();

		// console.log(nameInside); // only accessible on the function scope.

// [SECTION] Using alert () and prompt ()
	// alert( allows us to show small window at the top of our  browser page to show info to our users.

	//alert("Hello World"); // this will run immediately when the page reloads.

	// you can use alert() to show a message to the user from a later function invocation.

	function showSampleAlert(){
		alert("Hello, User");
	}

	// showSampleAlert();

	console.log("I will only log in the console when the alert is dismissed.");

	// Notes on the use of alert();

		// Show only an alert() for short dialogs/messages to the user.
		// do not overuse alert() because the program has to wait for it to be dismissed before it continues.

	//prompt() allows us to show a small window at the top of the broswer to gather user input.log
	// the input from the prompt() will return as a "String" once the user dismissed the window.

	/*
	
		Syntax: let/const variableName = prompt("<dialogInString>");
	*/

	/*let name = prompt("Enter your name: ");
	let age = prompt("Enter your age: ");
	console.log(typeof age);
	console.log("Hello Iam " + name + ", and I am " + age + " years old.");*/

	// let sampleNullPrompt = prompt("Do not enter anything");

	// console.log(sampleNullPrompt);
	// prompt() return an "empty string" ("") when there is no user input and we have clicked okay. or "null" if the user cancels the prompt.

	function printWelcomeMessage(){
		let name = prompt("Enter your name: ");

		console.log("Hello, "+name+"! Welcome to my page!");
	}

	printWelcomeMessage();

	// [SECTION] Function Naming conventions

		// 1. function names should be definitive of the task it will perform. it usually contains a verb.
			// function also follows camelCasing in naming variables

			function getCOurses(){
				let courses = ["Science 101", "Math 101", "English 101"];
				console.log(courses);
			}

			getCOurses();

		// 2. avoid generic names to avoid copnfusion within your code.

			function get(){
				let name = "jamie";
				console.log(name);
			}

			get();

		// 3. Avoid pointless and iappropriate function names, example: foo, bar (metasyntatic variable - placeholder variables).

			function foo(){
				console.log(25%5)
			}

			foo();